// @flow

// leave off @2x/@3x
const images = {
  nav_close: require('../../source/nav_Close@2x.png'),
    icon_biyan: require('../../source/icon_biyan@2x.png'),
    icon_guanbi: require('../../source/icon_guanbi@2x.png'),
    icon_mima: require('../../source/icon_mima@2x.png'),
    icon_shouji: require('../../source/icon_shouji@2x.png'),
    icon_weixuanzhong: require('../../source/icon_weixuanzhong@2x.png'),
    icon_xuanzhong: require('../../source/icon_xuanzhong@2x.png'),
    icon_yanjing: require('../../source/icon_yanjing@2x.png'),
    icon_yanzhenma: require('../../source/icon_yanzhenma@2x.png'),
    icon_youxiang: require('../../source/icon_youxiang@2x.png'),
    icon_zhanghao: require('../../source/icon_zhanghao@2x.png'),
    icon_return: require('../../source/icon_return@2x.png'),

}

export default images
