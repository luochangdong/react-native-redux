// @flow

const colors = {
  _20232b: '#20232b',
  _23262e: '#23262e',
    _414450: '#414450',
    _E0C294: '#E0C294',
    _F3DBB6: '#F3DBB6',
    _B7B7B7: '#B7B7B7',
    _747474: '#747474',
    _FFFFFF: '#FFFFFF',
    _2C3039: '#2C3039',

}

export default colors
