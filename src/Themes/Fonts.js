// @flow

const type = {
  base: 'HelveticaNeue',
  bold: 'HelveticaNeue-Bold',
  emphasis: 'HelveticaNeue-Italic'
}

const size = {
  h1: 38,
  h36: 36,
  h30: 30,
  h26: 26,
  h24: 24,
  h18: 18,
  input: 18,
  h15: 15,
  h14: 14,
  h12: 12,
  tiny: 8.5
}

const style = {
  h1: {
    fontFamily: type.base,
    fontSize: size.h1
  },
  h36: {
    fontSize: size.h36
  },
  h30: {
    fontSize: size.h30
  },
  h26: {
    fontSize: size.h26
  },
  h24: {
    fontSize: size.h24
  },
  h18: {
    fontSize: size.h18
  },
  normal: {
    fontFamily: type.base,
    fontSize: size.h15
  },
  description: {
    fontFamily: type.base,
    fontSize: size.h14
  }
}

export default {
  type,
  size,
  style
}

