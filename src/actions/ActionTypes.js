/**
 * Created by lorne on 2016/12/19.
 */



export const INDEX_LIST = 'INDEX_LIST';//登陆按钮

//for login
export const LOGGED_IN = "LOGGED_IN";
export const LOGGED_OUT = "LOGGED_OUT";
export const LOGGED_ERROR = "LOGGED_ERROR";
export const LOGGED_DING = "LOGGED_DING";

export const REGISTER_DOING = 'REGISTER_DOING';
export const REGISTER_OK = 'REGISTER_OK';
export const REGISTER_FAIL = 'REGISTER_FAIL';

//for fetching status
export const FETCH_START = 'FETCH_START';
export const FETCHING = 'FETCHING';
export const FETCH_SUCCESS = 'FETCH_SUCCESS';
export const FETCH_FAIL = 'FETCH_FAIL';


export const MOBILE_REGISTER = 'MOBILE_REGISTER';
export const EMAIL_REGISTER = 'EMAIL_REGISTER';