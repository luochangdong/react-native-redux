/**
 * Created by lorne on 2016/12/19.
 */
import * as types from './ActionTypes'

export function getAward(data) {
    return{
        type:types.AWARD_LIST,
        data:data
    }
}