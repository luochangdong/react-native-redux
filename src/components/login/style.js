/**
 * Created by lorne on 2016/12/23.
 */
import { StyleSheet } from 'react-native'
import Color from '../../styles/Colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        width: null,
        height: null,
        backgroundColor: Color.bg,
    },
    header:{
        backgroundColor:Color.bg,
    },
    title:{
       color:'white',
    },
    inputGroup:{
        borderBottomColor:Color.input_line,
        marginLeft:30,
        marginRight:45
    },

})