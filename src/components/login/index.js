/**
 * Created by lorne on 2016/12/23.
 */
import React, { Component } from 'react';
import {TouchableOpacity,Text,StyleSheet} from 'react-native';
import { connect } from 'react-redux';
import { Container, Header, Title, Content,Input, Button, Thumbnail,ListItem,InputGroup,View} from 'native-base';
import styles from './style'
import String from '../../styles/String'
import * as Action from '../../actions/LoginAction';



 class Login extends Component{

    constructor(props){
        super(props);

    }

     toMobileRegister = ()=>{
          const {router} = this.props;
          router.toRegisterMobile(this.props);
     }

     toEmailRegister = ()=>{
         const {router} = this.props;
         router.toHome();

     }
    getUsername = (text) =>{
         this.setState({username: text});
     }

     getPassword= (text) =>{
         this.setState({password: text});
     }

     login = () =>{
            this.props.loginByMobile(this.state.username,this.state.password)


     }


    render() {
        return (
            <Container style={styles.container}>
                <Header style={styles.header}>
                    <Button transparent>
                        <Thumbnail square size={15} source={require('../../../source/images/nav_close.png')} />
                    </Button>
                    <Title style={styles.title}>扑客</Title>
                </Header>

                <Content>
                    <InputGroup style={[styles.inputGroup,{marginTop:60}]}
                                borderType='underline'>
                            <Input
                                onChangeText={this.getUsername}
                                color='white'
                                placeholder={String.login_username}
                            />
                    </InputGroup>
                        <InputGroup style={styles.inputGroup}
                                    borderType='underline'>
                            <Input
                                onChangeText={this.getPassword}
                                placeholder={String.login_password}
                                secureTextEntry
                                color='white'
                            />
                        </InputGroup>
                    <TouchableOpacity style={myStyle.button}
                       onPress={this.login}>
                        <Text style={myStyle.text}>确定</Text>
                    </TouchableOpacity>

                    <Text style={[myStyle.text,{marginTop:20}]}
                     onPress={this.toMobileRegister}>手机注册</Text>
                    <Text style={[myStyle.text,{marginTop:10}]}
                    onPress={this.toEmailRegister}>邮箱注册</Text>
                </Content>


            </Container>
        );
    }
}
myStyle = StyleSheet.create({
    text:{
        textAlign:'center',
        color:'white'
    },
    button:{
         backgroundColor:'#E0C294',
         borderRadius:10,
         width:200,
         height:40,
         justifyContent:'center',
         alignSelf:'center',
         marginTop:30

    }
})



function bindAction(dispatch) {
    return {
        loginByMobile: (mobile,vcode) => dispatch(Action.loginByMobile(mobile,vcode))

    };
}
const mapStateToProps = state => ({
    data:state.LoginReducer.data,
    error:state.LoginReducer.error,
    status:state.LoginReducer.status,
    refresh:state.LoginReducer.refresh
});

export default connect(mapStateToProps,bindAction)(Login);