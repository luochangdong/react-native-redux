/**
 * Created by lorne on 2016/12/19.
 */

import React,{Component} from 'react';
import {Provider} from 'react-redux';
import configureStore from '../store/ConfigureStore';
import PuKe from './index';
import '../configs/StorageConfig';
import '../I18n/I18n'

const store = configureStore();

export default class App extends Component{

    render(){
        return(
            <Provider store={store}>
                <PuKe/>
            </Provider>
        )
    }

}