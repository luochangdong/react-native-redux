/**
 * Created by lorne on 2016/12/20.
 */
import Api from '../configs/ApiConfig'

const domain = Api.dev;

function filterStatus(res) {
    if(__DEV__) {
        console.log('status:' + res.status + '  statusText:' + res.statusText)
    }

    if (res.status >= 200 && res.status < 300) {
        return res
    }
    else {
        let error = new Error(res.statusText);
        error.res = res;
        error.type = 'http';
        throw error;
    }
}

function filterJSON(res) {
    if(__DEV__){
        console.log(res)
    }
    return res.json();
}

export function get(url){

    url =  domain+ url;
    if(__DEV__){
        console.info(`GET:` , url )
    }

    return fetch(url).then(filterStatus).then(filterJSON)
}

export function post(url, body ) {
    url =  domain+ url;

    if (__DEV__) {
        console.log(`POST: `, url);
        console.log(`Body: `, body)
    }


    const fetchBlob = fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    })
        .then(filterStatus)
        .then(filterJSON)
        .catch((error)=>{
          console.log(error)
        });

    return fetchBlob;
}