/**
 * Created by lorne on 2016/12/26.
 */
import * as helper from './RequestHelper';
import Api from '../configs/ApiConfig';

export function postRegisterByMobile(body,resolve,reject) {
    helper.post(Api.register,body,resolve,reject);
}

export function postRegisterByEmail(body, resolve, reject) {
    helper.post(Api.register,body,resolve,reject);
}

export function postLoginByMobile(body, resolve, reject) {
    helper.post(Api.login,body,resolve,reject);
}

export function postLoginByEmail(body, resolve, reject) {
    helper.post(Api.login,body,resolve,reject);
}