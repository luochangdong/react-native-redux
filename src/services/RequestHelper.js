/**
 * Created by lorne on 2016/12/23.
 */
import FetchHttpClient, { json } from 'fetch-http-client';
import Api from '../configs/ApiConfig';

let TAG = 'PuKeHttp:';
// Create a new client object.
export const client = new FetchHttpClient(Api.testing);
// Add json support
client.addMiddleware(json());

// Add Logging
client.addMiddleware(request => response => {

    console.log(request, response);
});

// Add access token asynchronously
client.addMiddleware(request => {
    request.options.headers['X-DP-APP-KEY'] = '467109f4b44be6398c17f6c058dfa7ee';
    request.options.headers['X-DP-CLIENT-IP'] = '192.168.2.231';
    request.options.headers['Content-Type'] = 'application/json';
});

export function post(url,body,resolve,reject) {
    console.log(url,body)
    client.post(url,{json:body})
        .then((response)=>{
          console.log(TAG,response.jsonData);
          const {code,msg} = response.jsonData;
          if(code === 0){
              resolve(response.jsonData);
          }else{
              reject(msg);
          }

        }).catch((error)=>{
        console.log(TAG,error);
        reject(error);
    });
}



