/**
 * Created by lorne on 2016/12/29.
 */
import * as TYPTES from '../actions/ActionTypes';
import * as LoginService from '../services/LoginService';

const initialState = {
    fetching:TYPTES.FETCH_START,
    user:{},
    error:{},
    apiType:TYPTES.MOBILE_REGISTER
};

export default function register(state = initialState,action) {
    switch (action.type){
        case TYPTES.MOBILE_REGISTER:
            console.log('.....register....',action.fetching)
            console.log(action.user)
            console.log(action.error)
            console.log('.....register....',action.fetching)
            return {
                ...state,
                fetching:action.fetching,
                user:action.user,
                error:action.error,
                apiType:action.type
            };
        default:
            return state;

    }
}

export function _registerFetchEmail(email, password) {
    const body = {
        type:'email',
        email:email,
        password:password
    };
    return (dispatch)=>{
        dispatch(_registerMobileRequest());
        LoginService.postRegisterByEmail(body,(data)=>{
            dispatch(_registerMobileSuccess(data));
        },(error)=>{
            dispatch(_registerMobileFail(error));
        })
    }
}


export function _registerFetchMobile(phone,vcode) {
    const body = {
        type:'mobile',
        mobile:phone,
        vcode:vcode
    };
    return (dispatch)=>{
        dispatch(_registerMobileRequest());
        LoginService.postRegisterByMobile(body,(data)=>{
            dispatch(_registerMobileSuccess(data));
        },(error)=>{
            dispatch(_registerMobileFail(error));
        })
    }
}

export function _registerMobileRequest() {
    return {
        type:TYPTES.MOBILE_REGISTER,
        fetching:TYPTES.FETCHING
    }
}

export function _registerMobileSuccess(user) {
    return {
        type:TYPTES.MOBILE_REGISTER,
        fetching:TYPTES.FETCH_SUCCESS,
        user:user
    }
}

export function _registerMobileFail(error) {
    return {
        type:TYPTES.MOBILE_REGISTER,
        fetching:TYPTES.FETCH_FAIL,
        error:error
    }
} 