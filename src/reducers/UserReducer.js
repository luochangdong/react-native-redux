/**
 * Created by lorne on 2016/12/20.
 */
import * as TYPES from '../actions/ActionTypes';

const initalState = {
    isLoggedIn:false,
    user:{},
    status:null,
}

export default function UserReducer(state=initalState,action) {
    switch (action.type){
        case TYPES.LOGGED_DING:
            console.log('reducer doing')
            return{
                ...state,
                status:'doing'
            };

        case TYPES.LOGGED_IN:
            console.log('reducer in')
            console.log(action.opt)
            return{
                ...state,
                isLoggedIn:true,
                user:action.user,
                status:'done'
            };

        case TYPES.LOGGED_OUT:
            console.log('reducer out')
            return{
                ...state,
                isLoggedIn:false,
                user:{},
                status:null
            };
        default:
            return state;
    }

}