/**
 * Created by lorne on 2016/12/23.
 */
import * as TYPES from '../actions/ActionTypes';
import * as Status from '../configs/constants';

const initialState = {
    isLoginIn:false,
    user:{},
    refresh:false
}

export default login = (state = initialState,action)=>{
    switch (action.type){
        case TYPES.LOGGED_DING:
            console.log('loginReducer doing')
           return {
               ...state,
               isLoginIn:false,

           };
        case TYPES.LOGGED_IN:
            console.log('loginReducer in:'+action.user)
            return {
                ...state,
                isLoginIn:true,
                user:action.user
            };
        case TYPES.REGISTER_DOING:
            return {
                ...state,
                status:Status.DOING,
                refresh:false
            };
        case TYPES.REGISTER_OK:
            return{
                ...state,
                status:Status.SUCCESS,
                data:action.data,
                refresh:true
            };
        case TYPES.REGISTER_FAIL:
            return{
                ...state,
                status:Status.FAIL,
                error:action.error,
                refresh:true
            };
        default:
            return state;
    }
}