/**
 * Created by lorne on 2016/12/19.
 */
import {combineReducers} from 'redux';

import Essence from './Essence';
import UserReducer from './UserReducer';
import LoginReducer from './LoginReducer';
import RegisterRedux from './RegisterRedux';

const rootReducer = combineReducers({
    UserReducer,
    Essence,
    LoginReducer,
    RegisterRedux

});

export default rootReducer;