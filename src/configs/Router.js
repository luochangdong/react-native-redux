/**
 * Created by lorne on 2016/12/20.
 */
import React,{Navigator} from 'react-native'

//Pages
import Login from '../components/login/'
import RegisterPage from '../pages/RegisterPage';
import HomePage from '../pages/HomePage';
import Register from '../pages/Register';
import InputPwdPage from '../pages/InputPwdPage';

//Config
const secenConfig = require('./SceneConfig')

const customFloatFromRight = secenConfig.customFloatFromRight;


export default class Router {
    constructor(navigator) {
        this.navigator = navigator
    }

    push(props, route) {
        let routesList = this.navigator.getCurrentRoutes()
        let nextIndex = routesList[routesList.length - 1].index + 1
        route.props = props
        route.index = nextIndex
        this.navigator.push(route)
    }


    pop() {
        this.navigator.pop()
    }


    toInputPwdPage(props,email){
        this.push(props, {
            page: InputPwdPage,
            name: 'InputPwdPage',
            sceneConfig: customFloatFromRight,
            params:{
                email:email
            }
        })
    }
    toInputPwdPage(props,phone,code){
        this.push(props, {
            page: InputPwdPage,
            name: 'InputPwdPage',
            sceneConfig: customFloatFromRight,
            params:{
                phone:phone,
                code:code
            }
        })
    }
    toRegister(props){
        this.push(props, {
            page: Register,
            name: 'Register',
            sceneConfig: customFloatFromRight
        })
    }

    toHome(props){
        this.push(props, {
            page: HomePage,
            name: 'HomePage',
            sceneConfig: customFloatFromRight
        })
    }



    toRegisterMobile(props){
        this.push(props, {
            page: RegisterPage,
            name: 'RegisterPage',
            sceneConfig: customFloatFromRight,
            params:{
                pageType:'mobile'
            }
        })
    }

    toRegisterEmail(props){
        this.push(props, {
            page: RegisterPage,
            name: 'RegisterPage',
            sceneConfig: customFloatFromRight,
            params:{
                pageType:'email'
            }
        })
    }


    replaceWithHome() {
        this.navigator.popToTop()
    }

    resetToLogin(){
        this.navigator.resetTo({
            name: 'Login',
            page: Login,
            // sceneConfig: customFloatFromRight,
        })
    }


}
