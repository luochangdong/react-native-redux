/**
 * Created by lorne on 2016/12/26.
 */
import React ,{Component} from 'react';
import {Text,TouchableOpactity,View,TextInput,StyleSheet} from 'react-native';
import { connect } from 'react-redux';
import { Container, Header, Title, Content,Button,Icon} from 'native-base';
import Color from '../styles/Colors';
import String from '../styles/String';
import * as Action from '../actions/LoginAction';
import * as Status from '../configs/constants';
var codeTime = 10;
class RegisterPage extends Component{

    constructor(props){
        super(props);
        this.state = {
            btnDisable:false,
            timerCount:codeTime,
            timerTitle:'获取验证码',
            pageType:'awdsfa'
        }
    }

    componentDidMount(){
        this.setState({
            pageType: this.props.pageType
        });
        console.log(this.props.params)
        console.log(this.props.name)
    }

    shouldComponentUpdate(nextProps){
        if(nextProps.status !== this.props.status){
            if(nextProps.status === Status.DOING){
                console.log(nextProps.status)
            }else if(nextProps.status === Status.SUCCESS){
                console.log(nextProps.status)
                console.log(nextProps.data)

            }else if(nextProps.status === Status.FAIL){
                console.log(nextProps.status)
                console.log(nextProps.error)
            }
        }

        return true;
    }

    componentWillUnmount(){
       this.interval && clearInterval(this.interval);
    }

    codeTimer = ()=>{
        this.interval=setInterval(() =>{

            var timer=this.state.timerCount-1
            if(timer===0){
                this.setState({btnDisable:false});
                this.interval&&clearInterval(this.interval);

                this.setState({
                    timerCount:codeTime,
                    timerTitle:'重新获取'
                })

            }else{
                this.setState({
                    timerCount:timer,
                    timerTitle:timer.toString()
                })

            }

        },1000)
    }

    postMobile = ()=>{

            this.setState({btnDisable:true});
            this.codeTimer();
            this.props.registerByMobile(this.state.mobile,this.state.password)


    }


    getMobile = (text)=>{
        this.setState({mobile:text})
    }

    getPassword = (text)=>{
        this.setState({password:text})
    }


    render(){
        const {router} = this.props;
            return (
                <Container>
                    <Header>
                        <Button onPress={()=>{
                            router.pop();
                        }}>
                        <Text>返回</Text>
                        </Button>
                        <Title>{String.login_register}</Title>
                    </Header>
                    <Content>

                        <View>
                            <View style={styles.register_input_bg}>
                            <TextInput style={styles.register_input}
                                       onChangeText={this.getMobile}
                                       keyboardType='numeric'
                                       maxLength={11}
                                       placeholder={String.register_phone}
                            />
                        </View>
                            <View style={styles.register_input_bg}>
                                <TextInput style={styles.register_input}
                                           onChangeText={this.getPassword}
                                           placeholder={String.vcode}
                                />
                            </View>

                            <Button block
                                    style={[styles.btn,this.state.btnDisable?styles.btnDisable:styles.btnAble]}
                                    disabled={this.state.btnDisable}
                                    onPress={this.postMobile}>
                                {this.state.timerTitle}
                            </Button>

                        </View>

                    </Content>

                </Container>
            )

    }
}

const styles = StyleSheet.create({
    register_input_bg:{
        borderBottomColor:Color.input_line,
        borderBottomWidth:1,

    },
    register_input:{
        height:40,
        color:'red',
        marginTop:15
    },
    btn:{
        margin:30,
        backgroundColor:'green'
    },
    btnDisable:{
        backgroundColor:'gray',

    },
    btnAble:{
        backgroundColor:'green'
    }
})

function bindAction(dispatch) {
    return {
        registerByMobile: (mobile,password) => dispatch(Action.registerByMobile(mobile,password)),
        registerByEmail:(email,password)=>dispatch(Action.registerByEmail(email,password))
    };
}
const mapStateToProps = state => ({
    data:state.LoginReducer.data,
    error:state.LoginReducer.error,
    status:state.LoginReducer.status,
    refresh:state.LoginReducer.refresh
});

export default connect(mapStateToProps,bindAction)(RegisterPage);