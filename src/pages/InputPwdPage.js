/**
 * Created by lorne on 2016/12/29.
 */
import React from 'react';
import {Text,TouchableOpacity,View,TextInput,StyleSheet,Image} from 'react-native';
import { Container, Header, Title, Content,Button} from 'native-base';
import { connect } from 'react-redux';
import {Colors,Fonts,Images} from '../Themes';
import I18n from 'react-native-i18n';

import * as RegisterRedux from '../reducers/RegisterRedux';

var phone ;
var code;
var email;
var password;
 class InputPwdPage extends React.Component{


     componentDidMount(){

         phone = this.props.params.phone;
         code= this.props.params.code;
         email = this.props.params.email;
         console.log(this.props.params)
     }
     _request_register = ()=>{
         if(email){
             this.props.registerByEmail(phone,code);

         }else{
             this.props.registerByMobile(email,password);
         }
     };
     _input_password_text = (text)=>{
         password = text.toString();
     };

    render(){
        return (
            <Container style={{backgroundColor:Colors._20232b}}>
                <Header style={{backgroundColor:Colors._20232b}}>
                    <Button transparent
                            onPress={()=>{this.props.router.pop()}}>
                        <Image style={{width:11,height:17}} source={Images.icon_return}/>
                    </Button>
                    <Title></Title>

                </Header>
               <Content>
                   <View style={[styles.view_input,{marginTop:65}]}>

                       <TextInput style={styles.text_input}
                                  placeholderTextColor={Colors._747474}
                                  onChangeText={this._input_password_text}
                                  placeholder={I18n.t('please_input_pwd')}/>
                       <Image  source={Images.icon_yanjing}
                               style={styles.img_eye}/>
                   </View>

                   <TouchableOpacity style={styles.btn_sign_in}
                     onPress={this._request_register}>
                       <Text style={styles.btn_text_sign}>{I18n.t('complete')}</Text>
                   </TouchableOpacity>

               </Content>

            </Container>
        )
    }
}

const styles = StyleSheet.create({

    btn_text_sign:{
        alignSelf:'center',
        color:Colors._414450
    },
    btn_sign_in:{
        alignSelf:'center',
        marginTop:44,
        backgroundColor:Colors._E0C294,
        height:45,
        width:278,
        justifyContent:'center'

    },

    img_eye:{alignItems:'flex-end',marginRight:10,width:17,height:10},
    text_input:{
        height:35,
        color:Colors._FFFFFF,
        flex:1,
        fontSize:15
    },
    view_input:{
        borderBottomColor:Colors._F3DBB6,
        borderBottomWidth:0.5,
        marginLeft:49,
        marginRight:49,
        flexDirection:'row',
        alignItems:'center',
    },

});

function bindAction(dispatch) {
    return {
        registerByMobile: (mobile,vcode) => dispatch(RegisterRedux._registerFetchMobile(mobile,vcode)),
        registerByEmail:(email,password)=>dispatch(RegisterRedux._registerFetchEmail(email,password))
    };
}
const mapStateToProps = state => ({
    apiType:state.RegisterRedux.apiType,
    fetching:state.RegisterRedux.fetching,
    user:state.RegisterRedux.user,
    error:state.RegisterRedux.error

});

export default connect(mapStateToProps,bindAction)(InputPwdPage);