/**
 * Created by lorne on 2016/12/20.
 */
import React,{Component} from 'react';
import {Navigator} from 'react-native'
import Router from '../configs/Router';
import LoginPage from './LoginPage'

let initialRoute = {
    name:'LoginPage',
    page:LoginPage,
}

export default class Root extends Component{
    constructor(props){
        super(props);

    }

    renderScene = ({page, name, id, index, props,params}, navigator)=>{
        this.router = this.router || new Router(navigator);
        if(page){
            return React.createElement(page, {
                ...props,
                ref: view => this[index] = view,
                router: this.router,
                name,
                params,
                route: {
                    name, id, index
                },
            })
        }
    }
    

    configureScene=(route)=>{
        if(route.sceneConfig){
            return route.sceneConfig;
        }
        return Navigator.SceneConfigs.FloatFromRight;
    }

    render() {
        return (<Navigator
            ref={view => this.navigator=view}
            initialRoute={initialRoute}
            configureScene={this.configureScene}
            renderScene={this.renderScene}
        />);
    }
}