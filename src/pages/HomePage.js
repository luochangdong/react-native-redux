/**
 * Created by lorne on 2016/12/27.
 */
import React, { Component } from 'react';
import {StyleSheet,Text,View,ListView} from 'react-native';
import { connect } from 'react-redux';
import { Container, Header, Title, Content, Button, Thumbnail} from 'native-base';
import String from '../styles/String';
import Color from '../styles/Colors';
import * as Images from '../../source/images/index';

export default class HomePage extends Component{
    constructor(props) {
        super(props);
        const opt = {date:'04/12月',title:'赛事名称',money:'奖池：￥34,234',address:'地点：海南三亚盘古酒店',until:'2016.12.12~2016.12.20'};
        const opt1 = {date:'04/12月',title:'赛事名称',money:'奖池：￥34,234'};
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows([opt, opt1, opt, opt, opt, opt, opt, opt
            ])
        };


    }

    itemRow = (rowData)=>{
        return (
            <View style={styles.home_list_view}>
                <View style={styles.column_line}/>
                <Text style={styles.item_text_month}>{rowData.date}</Text>
                <View style={styles.column_line_gray}/>
                <View style={styles.item_view_info}>
                    <Text style={styles.item_text_title}>{rowData.title}</Text>
                    <Text style={styles.item_text_money}>{rowData.money}</Text>
                    <Text style={styles.item_text_address}>{rowData.address}</Text>
                    <Text style={styles.item_text_until}>{rowData.until}</Text>
                </View>
            </View>
        )
    };

    render(){
        return (
            <Container style={ {backgroundColor:Color.bg}}>
                <Header
                    style={{backgroundColor:Color.input_line}}>
                  <Button>
                      <Thumbnail square  source={Images.icon_qq} />
                  </Button>
                    <Title style={{color:'white'}}>{String.deshpro}</Title>
                    <Button >
                        <Thumbnail square size={15} source={Images.icon_wx}/>
                    </Button>
                </Header>
                <Content >
                    {/*个人信息*/}
                    <View style={styles.person_view}>
                        <Thumbnail size={76} style={styles.person_avatar}/>

                        <Text style={styles.person_nick}>昵称</Text>
                        <Text style={styles.person_sign}
                          testSize="">个性签名</Text>
                       <View style={styles.person_feature}>
                           <Text style={styles.person_id}>ID:</Text>
                           <Text style={styles.person_id}>23049230</Text>
                           <Text style={styles.person_honer}>牌手称号</Text>
                       </View>
                    </View>
                    {/*功能模块*/}
                    <View style={styles.home_modules}>
                        <View style={styles.home_row}>
                            <View style={[styles.home_item,{backgroundColor:'red'}]}></View>
                            <View style={[styles.home_item,{backgroundColor:'green'}]}></View>
                            <View style={[styles.home_item,{backgroundColor:'red'}]}></View>
                        </View>
                        <View style={styles.home_row}>
                            <View style={[styles.home_item,{backgroundColor:'green'}]}></View>
                            <View style={[styles.home_item,{backgroundColor:'yellow'}]}></View>
                            <View style={[styles.home_item,{backgroundColor:'green'}]}></View>
                        </View>
                    </View>

                    {/*我的赛事*/}
                    <View>
                    <View style={styles.home_list1}>
                         <Text >我的赛事</Text>
                    </View>

                   <View style={styles.home_list2}>
                       <Text >即将到来的赛事</Text>
                   </View>
                    </View>
                    <ListView
                        dataSource={this.state.dataSource}
                        renderRow={this.itemRow}
                    />
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    item_view_info:{
        flex:0,
        flexDirection:'column',
        padding:10


    },
    item_text_until:{
        color:'white',
        marginTop:6,
        fontSize:9
    },
    item_text_address:{
        color:'white',
        marginTop:6,
        fontSize:12
    },
    item_text_money:{
        color:'white',
        marginTop:6,
        fontSize:15
    },
    item_text_title:{
        color:'white',
        fontSize:15
    },
    item_text_month:{
        marginLeft:10,
        marginRight:10,
        alignSelf:'center',
        color:'white'
    },
    column_line_gray:{
        alignSelf:'stretch',
        width:0.5,
        backgroundColor:'#333333',
        marginTop:4,
        marginBottom:4
    },
    column_line:{
        alignSelf:'stretch',
        width:4,
        backgroundColor:Color.input_line
    },
    home_list_view:{
        flex:0,//flex=0的项目占用空间仅为内容所需空间，flex=1的项目会占据其余所有空间
        marginTop:10,
        backgroundColor:'#222222',
        marginRight:15,
        marginLeft:15,        flexDirection:'row',
        alignItems:'center',
        minHeight:66
    },
    home_list2:{
        flex:1,
        height:50,
        marginRight:15,
        marginLeft:15,
        flexDirection:'row',
        alignItems:'center',
        backgroundColor:'yellow',
    },
    home_list1:{
        flex:1,
        height:60,
        marginTop:10,
        backgroundColor:'#222222',
        marginRight:15,
        marginLeft:15,
        flexDirection:'row',
        alignItems:'center'
    },
    home_item:{
        flex:1,
    },
    home_row:{
        flexDirection:'row',
        flex:1
    },
    home_modules:{
        height:219,
        backgroundColor:'#222222',
    },
    person_view:{
        height:206,
        backgroundColor:'#333333',
        alignItems:'center'
    },
    person_avatar:{
        backgroundColor:'red',
        marginTop:25
    },
    person_nick:{
        marginTop:11,
        color:'white',
        fontSize:18
    },
    person_sign:{
        color:'white',
        fontSize:12,
        marginTop:8
    },
    person_feature:{
        flexDirection:'row',
        marginTop:8,
    },
    person_id:{
        color:'white',
        fontSize:12
    },
    person_honer:{
        color:'white',
        fontSize:12
    }
})
