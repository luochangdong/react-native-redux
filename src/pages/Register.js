/**
 * Created by lorne on 2016/12/29.
 */
import React from 'react';
import {Text,TouchableOpacity,View,TextInput,StyleSheet,Image} from 'react-native';
import { Container, Header, Title, Content,Button,CheckBox} from 'native-base';
import { connect } from 'react-redux';
import {Colors,Fonts,Images} from '../Themes';
import I18n from 'react-native-i18n';
import {CountDownText }from '../components/countdown/CountDownText';
import Toast, {DURATION} from 'react-native-easy-toast';


export default class Register extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            canGetCode:false,
            checked:true,
            useEmailRegister:false,
            canNextDisabled:true,
            phone:'',
            code:'',
            email:'',
        };
    }

    _input_email = (text)=>{
        if(text.toString().length>0)
            this.setState({
                canNextDisabled:false
            });

        this.setState({
            email:text
        });

    };

    _input_code_text = (text) =>{
        if(text.toString().length>0)
            this.setState({
                canNextDisabled:false
            });
        this.setState({
            code:text
        });
    };

    _input_phone_text = (text)=>{
        this.setState({
            phone:text
        });
    };

    _switch_email_phone = ()=>{
        this.setState({
            useEmailRegister:!this.state.useEmailRegister
        })
    };
    _back =()=>{
        this.props.router.pop();
    };
    _can_get_code =()=>{
        console.log('_can_get_code')
        this.setState({
            canGetCode:false
        });
    };

    _get_code = ()=>{
        if(this.state.phone.length>0){
            this.setState({
                canGetCode:true
            });
            this.refs.countDownText.start();
        }else{
            this.refs.toast.show(I18n.t('please_input_phone'),DURATION.LENGTH_LONG);

        }

    };
    _next = ()=>{
        if(this.state.checked){
            if(this.state.useEmailRegister)
            this.props.router.toInputPwdPage(this.props,this.state.email)
            else
                this.props.router.toInputPwdPage(this.props,this.state.phone,this.state.code)
        }
        else
            this.refs.toast.show(I18n.t('need_agree'),DURATION.LENGTH_LONG);
    };

    _protocol = ()=>{
        this.setState({checked:!this.state.checked});
    };

    _view_code = ()=>{
        return (
            <View style={[styles.view_input,{marginTop:15}]}>
                <Image source={Images.icon_yanzhenma} style={styles.img_vcode}/>
                <TextInput style={styles.text_input}
                           placeholderTextColor={Colors._747474}
                           underlineColorAndroid='transparent'
                           onChangeText={this._input_code_text}
                           placeholder={I18n.t('please_input_code')}/>
                <TouchableOpacity onPress={this._get_code}
                                  disabled={this.state.canGetCode}>
                    <CountDownText style={{color:Colors._747474,fontSize:15}}
                                   countType='seconds' // 计时类型：seconds / date
                                   afterEnd={this._can_get_code} // 结束回调
                                   auto={false} // 自动开始
                                   timeLeft={10} // 正向计时 时间起点为0秒
                                   step={-1} // 计时步长，以秒为单位，正数则为正计时，负数为倒计时
                                   startText='获取验证码' // 开始的文本
                                   endText='获取验证码' // 结束的文本
                                   ref='countDownText'
                                   intervalText={(sec) => sec + '秒重新获取'} // 定时的文本回调
                    />
                </TouchableOpacity>
            </View>
        );
    };

    _view_input_phone = ()=>{
        return (
            <View style={styles.view_input}>
                <Image  style={styles.icon_phone} source={Images.icon_shouji}/>
                <TextInput style={styles.text_input}
                           underlineColorAndroid='transparent'
                           placeholderTextColor={Colors._747474}
                           onChangeText={this._input_phone_text}
                           ref="input_phone"
                           keyboardType="numeric"
                           placeholder={I18n.t('please_input_phone')}/>
                <TouchableOpacity onPress={()=>{this.refs.input_phone.clear()}}>
                <Image  style={styles.icon_close} source={Images.icon_guanbi}
                       />
                </TouchableOpacity>
            </View>
        )
    };

    _view_input_email = ()=>{
        return (
            <View style={styles.view_input}>
                <Image  style={{width:16,height:11}} source={Images.icon_youxiang}/>
                <TextInput style={styles.text_input}
                           placeholderTextColor={Colors._747474}
                           onChangeText={this._input_email}
                           underlineColorAndroid='transparent'
                           placeholder={I18n.t('please_input_email')}/>
                <Image ref="ref_input_email"
                    style={styles.icon_close} source={Images.icon_guanbi}/>
            </View>
        )
    };

    render(){
        return (
            <Container style={{backgroundColor:Colors._20232b}}>
                <Header style={{backgroundColor:Colors._20232b}}>
                    <Button transparent
                            onPress={this._back}>
                        <Image style={{width:11,height:17}} source={Images.icon_return}/>
                    </Button>
                    <Title style={styles.txt_title}>{I18n.t('register_with_phone')}</Title>
                </Header>
                <Content>
                    <View style={{marginTop:60}}>
                        {/*输入框 邮箱或手机*/}
                        {this.state.useEmailRegister?this._view_input_email():this._view_input_phone()}

                        {/*获取验证码*/}
                        {this.state.useEmailRegister?null:this._view_code()}

                        <Text style={styles.register_tine}>{I18n.t('register_tine')}</Text>

                        <TouchableOpacity style={styles.btn_sign_in}
                        onPress={this._next}
                        disabled={this.state.canNextDisabled}>
                            <Text style={styles.btn_text_sign}>{I18n.t('next')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this._back}>
                        <Text style={styles.text_have_account}>{I18n.t('i_have_account')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.btn_email_register}
                         onPress={this._switch_email_phone}>
                            <Text style={styles.btn_text_email_register}>
                                {this.state.useEmailRegister?I18n.t('phone_register'):I18n.t('email_register')}</Text>
                        </TouchableOpacity>

                        <View style={styles.view_protocol}>
                            <TouchableOpacity onPress={this._protocol}>
                                   <Image style={styles.check_view}
                                       source={ this.state.checked?Images.icon_xuanzhong:Images.icon_weixuanzhong}/>
                            </TouchableOpacity>
                            <Text style={styles.text_protocol}>{I18n.t('protocol')}</Text>
                        </View>

                    </View>
                    <Toast ref="toast"
                           position='center'
                           textStyle={{color:'white'}}/>
                </Content>

            </Container>
        )
    }
}

const styles = StyleSheet.create({
    check_view:{
        width:16,
        height:16
    },
    text_protocol:{
        fontSize:Fonts.size.h12,
        color:Colors._747474,
        marginLeft:15
    },
    view_protocol:{
        flexDirection:'row',
        marginTop:169,
        justifyContent:'center',
        alignItems:'center'
    },
    btn_text_email_register:{
        fontSize:Fonts.size.h15,
        color:Colors._E0C294,
        alignSelf:'center',

    },
    btn_email_register:{
        marginTop:32,
        height:45,
        width:278,
        alignSelf:'center',
        borderWidth:0.5,
        borderColor:Colors._E0C294,
        justifyContent:'center'
    },
    text_have_account:{
        alignSelf:'flex-end',
        color:Colors._E0C294,
        marginTop:11,
        marginRight:49,
        fontSize:Fonts.size.h12

    },
    register_tine:{
      marginTop:15,
        alignSelf:'center',
        fontSize:Fonts.size.h12,
        color:Colors._747474
    },
    icon_close:{
        width:11,
        height:11,
        marginRight:10
    },
    txt_title:{
        color:Colors._FFFFFF,
        fontSize:Fonts.size.h18,
        fontFamily:Fonts.type.bold,

    },
    icon_phone:{width:12,height:14},
    img_lock:{
        width:11,
        height:13
    },
    img_eye:{alignItems:'flex-end',marginRight:10,width:17,height:10},
    text_input:{
        height:35,
        color:Colors._FFFFFF,
        flex:1,
        marginLeft:15,
        fontSize:Fonts.size.h15
    },
    view_input:{
        borderBottomColor:Colors._F3DBB6,
        borderBottomWidth:0.5,
        marginLeft:49,
        marginRight:49,
        flexDirection:'row',
        alignItems:'center',
    },
    btn_text_sign:{
        alignSelf:'center',
        color:Colors._414450
    },
    btn_sign_in:{
        alignSelf:'center',
        marginTop:38,
        backgroundColor:Colors._E0C294,
        height:45,
        width:278,
        justifyContent:'center'

    },
});


