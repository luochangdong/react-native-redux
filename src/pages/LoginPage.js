/**
 * Created by lorne on 2016/12/28.
 */
import React from 'react';
import {Text,TouchableOpacity,View,TextInput,StyleSheet,Image} from 'react-native';
import { Container, Header, Title, Content,Button,Thumbnail} from 'native-base';
import { connect } from 'react-redux';
import {Colors,Fonts,Images} from '../Themes';
import I18n from 'react-native-i18n';

export default class LoginPage extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            is_sign_with_vcode:false,
            text_sign_with:I18n.t('sign_in_whit_phone'),
        }
    }
    _to_register_page = ()=>{
        const {router} = this.props;
        router.toRegister();
    };
    _press_sign_with_vcode =()=>{
        const {is_sign_with_vcode} = this.state;
         const text_sign_with1 = is_sign_with_vcode?
             I18n.t('sign_with_pass'):I18n.t('sign_in_whit_phone');
        this.setState({
            is_sign_with_vcode:!is_sign_with_vcode,
            text_sign_with:text_sign_with1
        })
    };

    _sign_with_password = ()=>{
        return(
            <View style={[styles.view_input,{marginTop:15}]}>
                <Image source={Images.icon_mima} style={styles.img_lock}/>
                <TextInput style={styles.text_input}
                           placeholderTextColor={Colors._747474}
                           placeholder={I18n.t('password')}/>
                <Image  source={Images.icon_yanjing}
                        style={styles.img_eye}/>
            </View>
        )
    };

    _sign_with_vcode = ()=>{
        return(
            <View style={[styles.view_input,{marginTop:15}]}>
                <Image source={Images.icon_yanzhenma} style={styles.img_vcode}/>
                <TextInput style={styles.text_input}
                           placeholderTextColor={Colors._747474}
                           placeholder={I18n.t('password')}/>
                <Text style={{color:Colors._747474,fontSize:15}}>{I18n.t('get_vcode')}</Text>
            </View>
        )
    };

    render(){

         const {is_sign_with_vcode} = this.state;

         return(
             <Container style={{backgroundColor:Colors._20232b}}>
                 <Header style={{backgroundColor:Colors._20232b}}>
                     <Button transparent>
                         <Thumbnail size={18} source={Images.nav_close}/>
                     </Button>
                     <Title style={styles.txt_title}>{I18n.t('app_name')}</Title>
                     <Button transparent
                              onPress={this._to_register_page}>
                         <Text style={styles.txt_register}>{I18n.t('register')}</Text>
                     </Button>
                 </Header>
                 <Content style={{flex:1}}>

                     <View style={{marginTop:220}}>
                     <View style={styles.view_input}>
                         <Image  style={styles.icon_person} source={Images.icon_zhanghao}/>
                         <TextInput style={styles.text_input}
                              placeholderTextColor={Colors._747474}
                            placeholder={I18n.t('phone_email')}/>
                     </View>

                         {is_sign_with_vcode?this._sign_with_vcode():this._sign_with_password()}

                         <TouchableOpacity style={styles.btn_sign_in}>
                             <Text style={styles.btn_text_sign}>{I18n.t('sign_in')}</Text>
                         </TouchableOpacity>

                         <Text style={styles.text_problem}>{I18n.t('problem_for_sign_in')}</Text>

                     </View>
                     <Text style={styles.text_other_sign}
                           onPress={this._press_sign_with_vcode} >{I18n.t('sign_in_whit_phone')}</Text>
                 </Content>

             </Container>
         )
    }
}

const styles = StyleSheet.create({
    img_vcode:{
        height:12,
        width:15
    },
    text_other_sign:{
        alignSelf:'center',
        marginTop:100,
        fontSize:12,
        color:Colors._E0C294,
    },
    text_problem:{
       fontSize:12,
        color:Colors._E0C294,
        alignSelf:'center',
        marginTop:20
    },
    btn_text_sign:{
        alignSelf:'center',
        color:Colors._414450
    },
    btn_sign_in:{
        alignSelf:'center',
        marginTop:43,
        backgroundColor:Colors._E0C294,
        height:45,
        width:278,
        justifyContent:'center'

    },
    icon_person:{width:13,height:14},
    img_lock:{
      width:11,
        height:13
    },
    img_eye:{alignItems:'flex-end',marginRight:10,width:17,height:10},
    text_input:{
        height:35,
        color:Colors._FFFFFF,
        flex:1,
        marginLeft:15,
        fontSize:15
    },
    view_input:{
        borderBottomColor:Colors._F3DBB6,
        borderBottomWidth:0.5,
        marginLeft:49,
        marginRight:49,
        flexDirection:'row',
        alignItems:'center',
    },
    txt_title:{
        color:Colors._FFFFFF,
        fontSize:Fonts.size.h18,
        fontFamily:Fonts.type.bold,
        marginTop:5

    },
    txt_register:{
        color:Colors._FFFFFF,
        fontSize:Fonts.size.h18,
    }


})